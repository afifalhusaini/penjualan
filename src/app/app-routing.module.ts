import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { StudentComponent } from './pages/student/student.component';
import { TeacherComponent } from './pages/teacher/teacher.component';
import { HarianComponent } from './pages/harian/harian.component';

const routes: Routes = [
	{
		path:'home',
		component:HomeComponent
	},
	{
		path:'student',
		component:StudentComponent
	},
	{
		path:'teacher',
		component:TeacherComponent
	},
        {
		path:'harian',
		component:HarianComponent
	}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
