import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(config: NgbModalConfig, private modalService: NgbModal) { }

  ngOnInit() {
  	this.getData();
  }

  dataSource:any=[];
  user:any={};

  getData()
  {
  		this.dataSource=[
  			{
  				kode: '1678976',
  				namabarang: 'Indomilk',
                                jumlah: '3 dus',
                                harga: '500k'
  			},
  			{
  				kode: '2334355',
  				namabarang: 'Indomie goreng',
                                jumlah: '6 dus',
                                harga: '130k'
  			},
  			{
  				kode: '5456786',
  				namabarang: 'Aqua Botol Besar',
                                jumlah: '8 dus',
                                harga: '500k'
  			},


  		];
  }


  open(content) {
    this.modalService.open(content);
  }


  saveData(user)
  {
  	console.log(user);
  	this.dataSource.unshift(user);
  	//this.user={};
  }

  deleteData(idx)
  {
  	var konfirmasi=confirm('Yakin akan menghapus data?');
  	if(konfirmasi==true)
  	{
  		this.dataSource.splice(idx,1);
  	}
  }


}
