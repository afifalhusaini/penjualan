import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-harian',
  templateUrl: './harian.component.html',
  styleUrls: ['./harian.component.css']
})
export class HarianComponent implements OnInit {

  constructor(config: NgbModalConfig, private modalService: NgbModal) { }

  ngOnInit() {
  	this.getData();
  }

  dataSource:any=[];
  user:any={};

  getData()
  {
  		this.dataSource=[
  			{
  				id: '1678976',
                                kasir: 'rani',
  				namabarang: 'Indomilk',
                                jumlah: '3 ',
                                harga: '12000'
  			},
  			{
  				id: '1678976',
                                kasir: 'rani',
  				namabarang: 'Indomilk',
                                jumlah: '1 bungkus',
                                harga: '3500'
  			},
  			{
                                id: '1678976',
                                kasir: 'rani',
  				namabarang: 'Aqua Botol Besar',
                                jumlah: '1 ',
                                harga: '6000'
  			},


  		];
  }


  open(content) {
    this.modalService.open(content);
  }


  saveData(user)
  {
  	console.log(user);
  	this.dataSource.unshift(user);
  	//this.user={};
  }

  deleteData(idx)
  {
  	var konfirmasi=confirm('Yakin akan menghapus data?');
  	if(konfirmasi==true)
  	{
  		this.dataSource.splice(idx,1);
  	}
  }


}
