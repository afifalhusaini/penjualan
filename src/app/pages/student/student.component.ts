import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  constructor(config: NgbModalConfig, private modalService: NgbModal) { }

  ngOnInit() {
  	this.getData();
  }

  dataSource:any=[];
  user:any={};

  getData()
  {
  		this.dataSource=[
  			{
  				tanggal: '01-januari-2019',
  				pemasukan: '10 jt',
                                pengeluaran: '5 jt',
                                total: '5 jt'
  			},
  			{
  				tanggal: '01-februari-2019',
  				pemasukan: '10 jt',
                                pengeluaran: '4 jt',
                                total: '6 jt'
  			},
  			{
  				tanggal: '01-maret-2019',
  				pemasukan: '10 jt',
                                pengeluaran: '5 jt',
                                total: '5 jt'
  			},


  		];
  }


  open(content) {
    this.modalService.open(content);
  }


  saveData(user)
  {
  	console.log(user);
  	this.dataSource.unshift(user);
  	//this.user={};
  }

  deleteData(idx)
  {
  	var konfirmasi=confirm('Yakin akan menghapus data?');
  	if(konfirmasi==true)
  	{
  		this.dataSource.splice(idx,1);
  	}
  }


}
