import { Component, OnInit } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {

  constructor(config: NgbModalConfig, private modalService: NgbModal) { }

  ngOnInit() {
  	this.getData();
  }

  dataSource:any=[];
  user:any={};

  getData()
  {
  		this.dataSource=[
  			{
  				kode: '1678976',
                                tanggal: '01-january-2019',
  				namabarang: 'Indomilk',
                                jumlah: '1 dus'
                                
  			},
  			{
  				kode: '1678976',
                                tanggal: '01-january-2019',
  				namabarang: 'indomie goreng',
                                jumlah: '0 dus'
                                
  			},
  			{
  				kode: '5456786',
                                tanggal: '01-january-2019',
  				namabarang: 'Aqua Botol Besar',
                                jumlah: '1 dus'
                                
  			},


  		];
  }


  open(content) {
    this.modalService.open(content);
  }


  saveData(user)
  {
  	console.log(user);
  	this.dataSource.unshift(user);
  	//this.user={};
  }

  deleteData(idx)
  {
  	var konfirmasi=confirm('Yakin akan menghapus data?');
  	if(konfirmasi==true)
  	{
  		this.dataSource.splice(idx,1);
  	}
  }


}
