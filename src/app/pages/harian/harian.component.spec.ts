import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HarianComponent } from './harian.component';

describe('HarianComponent', () => {
  let component: HarianComponent;
  let fixture: ComponentFixture<HarianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HarianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HarianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
